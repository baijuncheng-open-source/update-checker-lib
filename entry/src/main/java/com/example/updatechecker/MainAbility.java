package com.example.updatechecker;

import com.example.updatecheckerlib.UpdateChecker;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.TextField;
import ohos.agp.components.Text;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.window.dialog.CommonDialog;
import ohos.bundle.IBundleManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbility extends Ability {

    private static final int UPDATE_BLUR = 1001;
    private static final int ERROR_MESSAGE = 1002;
    static String getMessage;
    public EventHandler mWorkHandler = new EventHandler(EventRunner.create("work thread"));
    EventHandler mUIHandler = new EventHandler(EventRunner.getMainEventRunner()) {
        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            switch (event.eventId) {
                case UPDATE_BLUR:
                    successMessage((UpdateChecker.UpdateInfo) event.object);
                    break;
                case ERROR_MESSAGE:
                    errorMessage();
                    break;
                default:
                    break;
            }
        }
    };
    TextField mEditTextPackageName;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        boolean openInternet = verifySelfPermission("ohos.permission.INTERNET") == IBundleManager.PERMISSION_GRANTED;
        mEditTextPackageName = (TextField) findComponentById(ResourceTable.Id_editText_packagename);
        Button button1 = (Button) findComponentById(ResourceTable.Id_button_coolapk);
        button1.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mWorkHandler.removeAllEvent();
                mWorkHandler.postTask(new CheckTask(UpdateChecker.Market.MARKET_COOLAPK, mEditTextPackageName.getText().toString()));
            }
        });
        Button button2 = (Button) findComponentById(ResourceTable.Id_button_googleplay);
        button2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mWorkHandler.removeAllEvent();
                mWorkHandler.postTask(new CheckTask(UpdateChecker.Market.MARKET_GOOGLEPLAY, mEditTextPackageName.getText().toString()));
            }
        });
        Button button3 = (Button) findComponentById(ResourceTable.Id_button_wandoujia);
        button3.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mWorkHandler.removeAllEvent();
                mWorkHandler.postTask(new CheckTask(UpdateChecker.Market.MARKET_WANDOUJIA, mEditTextPackageName.getText().toString()));
            }
        });
    }

    class CheckTask implements Runnable {
        UpdateChecker.Market mMarket;
        String mPkg;

        public CheckTask(UpdateChecker.Market market, String pkg) {
            mMarket = market;
            mPkg = pkg;
        }

        @Override
        public void run() {
            executeCheck();
        }

        protected void executeCheck() {
            try {
                final UpdateChecker.UpdateInfo info = UpdateChecker.check(mMarket, mPkg);
                if (info != null) {
                    onPostExecute(info);
                }
            } catch (Exception e) {
                getMessage = e.getMessage();
                onPostExecute(null);
            }

        }

        protected void onPostExecute(UpdateChecker.UpdateInfo info) {
            InnerEvent innerEvent;
            if (info != null) {
                innerEvent = InnerEvent.get(UPDATE_BLUR);
            } else {
                innerEvent = InnerEvent.get(ERROR_MESSAGE);
            }
            innerEvent.object = info;
            mUIHandler.sendEvent(innerEvent);
        }
    }

    public void successMessage(UpdateChecker.UpdateInfo info) {
        CommonDialog builder = new CommonDialog(MainAbility.this);
        Component component = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dialog_message, null, false);
        Text text = (Text) component.findComponentById(ResourceTable.Id_text);
        text.setText("最新版本名称：" + info.getVersionName() + "\n" + "最新版本号：" + info.getVersionCode() + "\n" + "更新日志：" + "\n" + info.getChangeLog());
        DirectionalLayout title = (DirectionalLayout) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_dialog_title, null, false);
        Text titleTxt = (Text) title.findComponentById(ResourceTable.Id_title_txt);
        titleTxt.setText("检查结果");
        builder.setTitleCustomComponent(title)
                .setContentCustomComponent(component)
                .setSize(1000, 1000)
                .show();
    }

    private void errorMessage() {
        CommonDialog builder = new CommonDialog(MainAbility.this);
        builder.setTitleText("检查结果")
                .setContentText(getMessage)
                .setSize(1000, 1000)
                .show();
    }
}
