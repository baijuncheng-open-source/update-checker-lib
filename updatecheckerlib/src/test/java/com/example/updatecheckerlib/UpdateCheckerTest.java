package com.example.updatecheckerlib;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.example.updatecheckerlib.UpdateChecker.Market.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.spy;

public class UpdateCheckerTest {


    UpdateChecker updateChecker;
    UpdateChecker.UpdateInfo info ;

    UpdateChecker.UpdateInfo updateInfo;
    @Before
    public void setUp() throws Exception {

        updateChecker = new UpdateChecker();
        updateInfo = mock(UpdateChecker.UpdateInfo.class);
        info = mock(UpdateChecker.UpdateInfo.class);

    }

    @After
    public void tearDown() throws Exception {


    }
    @Test
    public void getChangeLog()throws Exception{
        UpdateChecker updateChecker2 = spy(updateChecker);
        String mChangeLog = updateChecker2.check(MARKET_COOLAPK,"com.p1.mobile.putong").getChangeLog();
        assertNotNull(mChangeLog);
    }
    @Test
    public void getMarket()throws IOException{
        UpdateChecker updateChecker2 = spy(updateChecker);
        UpdateChecker.Market aaaMarket = updateChecker2.check(MARKET_COOLAPK,"com.p1.mobile.putong").getMarket();
        assertNotNull(aaaMarket);

    }
    @Test
    public void getPackageName()throws Exception{
        UpdateChecker updateChecker2 = spy(updateChecker);
        String mChangeLog= updateChecker2.check(MARKET_COOLAPK,"com.p1.mobile.putong").getPackageName();
        assertNotNull(mChangeLog);
    }
    @Test
    public void getVersionName()throws Exception{
        UpdateChecker updateChecker2 = spy(updateChecker);
        String mVersionName = updateChecker2.check(MARKET_COOLAPK,"com.p1.mobile.putong").getVersionName();
        assertNotNull(mVersionName);
    }
    @Test
    public void getVersionCode()throws Exception{
        UpdateChecker updateChecker2 = spy(updateChecker);
        String mVersionCode = updateChecker2.check(MARKET_COOLAPK,"com.p1.mobile.putong").getVersionCode();
        assertNotNull(mVersionCode);
    }



    @Test
    public void check() throws IOException {
        UpdateChecker updateChecker1 = spy(updateChecker);
        updateChecker1.check(MARKET_COOLAPK,"com.p1.mobile.putong");
        assertNotNull(updateChecker1.check(MARKET_COOLAPK,"com.p1.mobile.putong"));
    }


    @Test(expected = java.net.ConnectException.class)
    public void check2() throws IOException {
        UpdateChecker updateChecker1 = spy(updateChecker);
        updateChecker1.check(MARKET_GOOGLEPLAY,"6202222");


    }

    @Test(expected = java.lang.Exception.class)
    public void check1() throws IOException {
        UpdateChecker updateChecker1 = spy(updateChecker);
        updateChecker1.check(MARKET_WANDOUJIA,"6202222");
        assertNotNull(updateChecker1.check(MARKET_WANDOUJIA,"6202222"));

    }

}












