package com.example.updatecheckerlib;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class UpdateChecker {
    public static class UpdateInfo {
        private Market mMarket;
        private String mPackageName;
        private String mChangeLog;
        private String mVersionName;
        private String mVersionCode;

        public String getChangeLog() {
            return mChangeLog;
        }

        public Market getMarket() {
            return mMarket;
        }

        public String getPackageName() {
            return mPackageName;
        }

        public String getVersionName() {
            return mVersionName;
        }

        public String getVersionCode() {
            return mVersionCode;
        }

        private UpdateInfo() {
        }
    }

    public enum Market {
        MARKET_COOLAPK,
        MARKET_GOOGLEPLAY,
        MARKET_WANDOUJIA
    }

    public static UpdateInfo check(Market market, String packageName) throws IOException {
        Document document;
        UpdateInfo info = new UpdateInfo();
        info.mMarket = market;
        info.mPackageName = packageName;
        switch (market) {
            case MARKET_COOLAPK:
                String urlCoolapk = "https://coolapk.com/apk/";
                document = Jsoup.connect(urlCoolapk + packageName).get();
                String documentTitle = document.select("title").text();
                int nameIndex = documentTitle.indexOf("(");
                info.mVersionName = documentTitle.substring(0, nameIndex);
                info.mChangeLog = document.select("div.warpper").select("div.container").select("div.app_left").select("div.apk_left_two").select("div.apk_left_two_box").select("div.apk_left_title").select("p.apk_left_title_info").html();
                int CodeIndex1 = documentTitle.indexOf("-");
                int CodeIndex2 = documentTitle.indexOf("-", CodeIndex1 + 1);
                info.mVersionCode = documentTitle.substring(CodeIndex1 + 2, CodeIndex2 - 1);
                return info;
            case MARKET_GOOGLEPLAY:
                String urlGoogleplay = "https://play.google.com/store/apps/details?id=";
                document = Jsoup.connect(urlGoogleplay + packageName).get();
                info.mVersionName = document.select("div[itemprop=softwareVersion]").get(0).html();
                Elements elements = document.select("div.recent-change");
                StringBuffer sb=new StringBuffer();
                for (Element element : elements) {
                    sb.append(element.text());
                }
                info.mChangeLog = sb.toString();
                return info;
            case MARKET_WANDOUJIA:
                String urlWandoujia = "https://www.wandoujia.com/apps/";
                document = Jsoup.connect(urlWandoujia + packageName).get();
                info.mVersionName = document.select("div[class=con]").get(0).text().substring(3);
                info.mChangeLog = document.select("div[class=con]").get(1).html();
                return info;
        }
        return null;
    }
}
